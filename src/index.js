// ==================== MANERA ANTERIOR ================================
/*
// CREAR CONSTANTE QUE ALMANCENA UN ELEMENTO = h1
const element = document.createElement('h1')
// INSERTO TEXTO innerText en el elemento que cree -> element
element.innerText = 'Hello React'
// CREAR CONSTANTE/CONTENEDOR EN UN ID = root (div de index.html)
const container = document.getElementById('root')
// IMPRIMO EL element que tiene el texto en el contenedor root (div)
container.appendChild(element)
*/

// ==================== REACT JS ========================================

import React from 'react'
import ReactDOM from 'react-dom'

const user = {
    PrimerNombre: 'Juan',
    SegundoNombre: 'Galvis',
    avatar: 'https://mteheran.files.wordpress.com/2017/06/logo-578x270.png?w=578'
}

function getName(user)
{
    return user.PrimerNombre +' '+ user.SegundoNombre;
}

// FUNCIÓN
function getGreeting(user)
{
// CONDICIONAL EN JSX / REACT JS
   if (user) {
    return <h1>Hello {getName(user)}</h1> //JSX

   }else
    {
        return <h1>Hello Strange</h1> //JSX
    }
}

// CREAR ELEMENTO CON VARIOS "HIJOS" O COMPONENTES
const element = (
    <div>
        <h1>{getGreeting(user)}</h1>
        <img src={user.avatar} alt="Imagen de Prueba"  />

    </div>
)
const container = document.getElementById('root')

// ReactDOM.render(__QUE__, __DONDE__)
ReactDOM.render(element, container)

// ============================================================================